package com.example.hellospringboot.controller;

import com.example.hellospringboot.bean.TestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @Autowired
    private TestBean getBean;

    @GetMapping("/nihao")
    public String getHello() {
        System.out.println(getBean.getName());
        System.out.println(getBean.getAge());
        System.out.println(getBean);
        return "nihao!";
    }

    @GetMapping("/nihao2")
    public Map<String,String> getHello2(@RequestHeader Map<String,String> map) {
        map.forEach((set,set2)-> System.out.println(set+"------"+set2));
        return map;
    }
}
