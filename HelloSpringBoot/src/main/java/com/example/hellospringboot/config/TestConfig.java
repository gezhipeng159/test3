package com.example.hellospringboot.config;

import com.example.hellospringboot.bean.TestBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {

    @Bean
    public TestBean getBean(){

        return new TestBean("test",11);

    }


}
