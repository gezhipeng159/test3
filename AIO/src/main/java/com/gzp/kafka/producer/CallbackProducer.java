package com.gzp.kafka.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class CallbackProducer {

    //配置生产者文件
    public static Properties setProperties() {
        //创建kafka生产者信息
        Properties properties = new Properties();
        //指定连接的kafka集群
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "kafkaTest01:9092");
        //k v序列化使用类
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        return properties;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //创建生产者信息
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(setProperties());

        for (int i = 0; i < 10; i++) {
            Future<RecordMetadata> send = producer.send(new ProducerRecord<>("rrr", 0, "key", "gzzzpp" + i), (recordMetadata, e) -> {
                if (e == null) {
                    System.out.println(recordMetadata.offset() + "---" + recordMetadata.partition());
                } else {
                    e.printStackTrace();
                }
            });
            send.get();
        }
        producer.close();
    }

}
