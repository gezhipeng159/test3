package com.gzp.kafka.consumer;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.ListConsumerGroupsResult;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.clients.admin.TopicListing;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class MyConsumer {
    //配置消费者文件
    public static AdminClient adminClient() {
        Properties properties = new Properties();
        //连接的集群
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.121.100:9092");
        //设置自动提交
        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        //提交的延迟
        properties.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        //kv反序列化
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        //消费者组
//        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "testGzp1");
        //重置消费者的offset
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return AdminClient.create(properties);
    }


    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        AdminClient adminClient = adminClient();


        DescribeTopicsResult describeTopicsResult = adminClient.describeTopics(Arrays.asList("gzptopic02"));
        Map<String, TopicDescription> stringTopicDescriptionMap = describeTopicsResult.all().get();
        Set<Map.Entry<String, TopicDescription>> entries = stringTopicDescriptionMap.entrySet();
        entries.stream().forEach((entry) -> {
            System.out.println("name ：" + entry.getKey() + " , desc: " + entry.getValue().partitions().size());

        });


        // 是否查看internal选项
        ListTopicsOptions options = new ListTopicsOptions();
        options.listInternal(true);
//        ListTopicsResult listTopicsResult = adminClient.listTopics();
        ListTopicsResult listTopicsResult = adminClient.listTopics(options);
        Set<String> names = listTopicsResult.names().get();
        Collection<TopicListing> topicListings = listTopicsResult.listings().get();
        KafkaFuture<Map<String, TopicListing>> mapKafkaFuture = listTopicsResult.namesToListings();
        // 打印names
//        names.stream().forEach(System.out::println);
        // 打印topicListings
//        topicListings.forEach(System.out::println);

        customConsumer("gzptopic02", "gzptestcg");

        ListConsumerGroupsResult listConsumerGroupsResult = adminClient.listConsumerGroups();
        listConsumerGroupsResult.all().get().forEach(s -> {
            System.out.println(s.groupId());
            System.out.println(s);
        });

        ListConsumerGroupOffsetsResult result = adminClient.listConsumerGroupOffsets("gzptestcg");
        Map<TopicPartition, OffsetAndMetadata> ma = result.partitionsToOffsetAndMetadata().get();
        System.out.println("ssssssss" + ma);
    }

    public static void customConsumer(String topicName, String groupId) {
        Properties properties = new Properties();
        //连接的集群
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "node02:9092");
        //消费组
        properties.setProperty("group.id", groupId);

        //1.还在同一个消费组消费但是之前的最小可消费offset已经不存在
        // 或者换了一个组去消费

        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        //关闭自动提交offset
        //  properties.put("enable.auto.commit", "false");
        //开启自动提交
        //properties.put("enable.auto.commit", "true");
        //  properties.put("auto.commit.interval.ms", "1000");
        //反序列化key val
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");


        //创建消费者
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);

        ////订阅主题指定topic
        consumer.subscribe(Arrays.asList(topicName));
        //consumer.subscribe(Arrays.asList("bigdata2","bigdata3"));

        ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
        System.out.println(records.count());
    }

}
